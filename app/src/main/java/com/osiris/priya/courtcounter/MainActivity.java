package com.osiris.priya.courtcounter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    int scoreTeamA = 0;
    int scoreTeamB = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        displayForTeamA(0);
    }
   /*
   *  This method is for 3point display for Team A
   */
    public void displayThreePointsTeamA(View view){
        scoreTeamA = scoreTeamA + 3;
        displayForTeamA(scoreTeamA);
    }
    /*
    * This method is for 2point display for Team A
    */
    public void displayTwoPointTeamA(View v){
        scoreTeamA = scoreTeamA + 2;
        displayForTeamA(scoreTeamA);
    }
    /*
    *  This method is for displaying 1point for Free throw for Team A
    */
    public  void displayFreeThrowTeamA(View view){
        scoreTeamA++;
        displayForTeamA(scoreTeamA);
    }
    /**
     * Displays the given score for Team A.
     */
    public void displayForTeamA(int score) {
        final TextView scoreView = (TextView) findViewById(R.id.team_a_score);
        scoreView.setText(String.valueOf(score));
    }
    /*
    *   Methods for Team B
    */
    /*
   *  This method is for 3point display for Team B
   */
    public void displayThreePointsTeamB(View view){
        scoreTeamB = scoreTeamB + 3;
        displayForTeamB(scoreTeamB);
    }
    /*
    * This method is for 2point display for Team B
    */
    public void displayTwoPointTeamB(View v){
        scoreTeamB = scoreTeamB + 2;
        displayForTeamB(scoreTeamB);
    }
    /*
    *  This method is for displaying 1point for Free throw for Team B
    */
    public  void displayFreeThrowTeamB(View view){
        scoreTeamB++;
        displayForTeamB(scoreTeamB);
    }
    /*
    *  Displays score for Team B
    */
    public void displayForTeamB(int score_teamb) {
        final TextView scoreView_b = (TextView) findViewById(R.id.team_b_score);
        scoreView_b.setText(String.valueOf(score_teamb));
    }
    /*
    *  This method can set both scores to zero on reset
    */
    public void resetScores(View v){
        scoreTeamA = 0;
        scoreTeamB = 0;
        displayForTeamA(scoreTeamA);
        displayForTeamB(scoreTeamB);
    }
}
